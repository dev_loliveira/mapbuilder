import os
from pygame.locals import *

class EngineProperties():
    def __init__(self):
        self.screen                           =    None
        self.screen_size                      =    ( 480, 800 )
        self.base_dir                         =    os.path.dirname(os.path.abspath(__file__))
        self.loaded_images                    =    []
        
        self.accepted_image_extensions        =    [ "png", "jpg", "gif", "bmp" ]
        self.resource_dirs                    =    [ "imgs" ]
        
        self.selected_image_index             =    0
        self.selected_tile                    =    None
        self.multi_selected_tiles             =    []
        self.main_loop_delay                  =    10
        self.finished                         =    False 
        self.bg_color                         =    (0, 0, 0)
        self.bg_editmode_color                =    (200, 200, 200)
        self.grade_editmode_color             =    (0, 0, 0)
        self.editmode_grade_size              =    70
        self.tile_focus_color                 =    (255, 0, 0)
        self.grade_screen                     =    True
        self.key_edit_mode                    =    K_e
    
    def get_selected_image(self):
        if self.selected_image_index < len(self.loaded_images):
            return (self.loaded_images[self.selected_image_index])
        return (None)
    
    def multi_tile_selection(self, coords):
        img = self.get_selected_image()
        if not img: return (None)
        
        selected_tiles = []
        for coord in coords:
            for tiledict in img.tiles:
                if coord[0] >= tiledict["xcoord"] and coord[0] <= tiledict["xcoord"]+img.tile_size[0]:
                    if coord[1] >= tiledict["ycoord"] and coord[1] <= tiledict["ycoord"]+img.tile_size[1]:
                        if tiledict not in selected_tiles:
                            selected_tiles.append(tiledict)
                        break
        
        self.multi_selected_tiles = selected_tiles
        
        return (self.multi_selected_tiles)
