#!/usr/bin/env python
# *-* coding:utf-8 *-*

import sys
from main import main
import arguments_parser as args

'''
    Ferramenta para criacao de maps 2D dinamicamente
    O principal objetivo e torna-la capaz de:
        1) Ler todas as imagens de um diretorio
        
        2) Permitir a criar uma imagem utilizando tiles
           de qualquer uma das imagens encontradas
           Para isso sera necessario:
               a) selecionar uma das imagens encontradas
               b) selecionar um dos tiles que a compoe
               c) adicionar/remover esse tile da imagem
                  sendo editada
           
        3) Salvar imagem sendo editada
'''

if __name__ == "__main__":
    args_list = [
        "with-sound",
        "verbose",
        "debug="
    ]
    args_dict_help = {
        "with-sound": "Habilita a saida de som",
        "verbose":    "Habilita mensagens de status da aplicacao",
        "debug":      "Habilita ou desabilita modo debug [on/off]"
    }
    args_dict = {}
    if len(sys.argv[1:]):
        args_dict = args.parse(sys.argv[1:], args_list)
    else:
        print("Arguments:")
        for key in args_list:
            keyname = key.replace("=", "")
            print( ("--%s\t\t%s")%(keyname, args_dict_help[keyname]) )
    main(args_dict)

