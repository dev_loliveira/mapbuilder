import pygame

class ImageTiles():
    '''
        Representa uma imagem em memoria
        com todos os tiles que a compoe
    '''
    def __init__(self, datadict):
        self.image = datadict["image"]
        self.tile_size = (datadict["tile_size"], datadict["tile_size"])
        self.colorkey = datadict["colorkey"] if "colorkey" in datadict else (255, 0, 255)
        self.tiles = self.get_tiles()
    
    def get_tiles(self):
        '''
            @return Array
            @description
                Retorna um array de dicionarios contendo:
                    tile: imagem no formato tile
                    xcoord: coordenada x na tela desse tile
                    ycoord: coordenada y na tela desse tile
        '''
        tiles = []
        tile_size = self.tile_size
        width, height = self.image.get_width(), self.image.get_height()
        x, y = 0, 0
        
        for y in range(0, height, tile_size[1]):
            for x in range(0, width, tile_size[0]):
                surface = pygame.Surface(tile_size)
                surface.set_colorkey(self.colorkey)
                surface.blit(
                    self.image,
                    (0, 0),
                    (x, y, tile_size[0], tile_size[1])
                )
                tiles.append({
                    "tile": surface,
                    "xcoord": x, "xcoord-orig": x,
                    "ycoord": y, "ycoord-orig": y
                })
        
        return (tiles)
    
    def get_focused_tile(self, mousecoord):
        for tile in self.tiles:
            if mousecoord[0] >= tile["xcoord"] and mousecoord[0] <= tile["xcoord"]+self.tile_size[0]:
                if mousecoord[1] >= tile["ycoord"] and mousecoord[1] <= tile["ycoord"]+self.tile_size[1]:
                    return (tile)
        return (None)
    
    def move_tileset_left(self):
        for tile in self.tiles:
            tile["xcoord"] -= self.tile_size[0]
            
    def move_tileset_rigth(self):
        for tile in self.tiles:
            tile["xcoord"] += self.tile_size[0]
            
    def move_tileset_up(self):
        for tile in self.tiles:
            tile["ycoord"] -= self.tile_size[1]
            
    def move_tileset_down(self):
        for tile in self.tiles:
            tile["ycoord"] += self.tile_size[0]
    
    def reset_tileset_coord(self):
        for tile in self.tiles:
            tile["xcoord"] = tile["xcoord-orig"]
            tile["ycoord"] = tile["ycoord-orig"]
    
    def draw(self, engine_properties):
        screen = engine_properties.screen
        for datadict in self.tiles:
            screen.blit( datadict["tile"], (datadict["xcoord"], datadict["ycoord"]) )
            if engine_properties.grade_screen:
                pygame.draw.rect(screen, (0,255,0), (datadict["xcoord"], datadict["ycoord"], self.tile_size[0], self.tile_size[1]), 1)

