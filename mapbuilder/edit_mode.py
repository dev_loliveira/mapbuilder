import pygame
from pygame.locals import *
from mapbuilderutils import get_first_tile


class EditModeCell():
    def __init__(self, coords):
        self.coords = coords
        self.tiles = []


class EditModeTileManagement():
    def __init__(self, size, tile_size):
        self.size = size
        self.tile_size = tile_size
        self.grid_visible = True
        self.cells = []
        self.output_extension = "bmp"
        self.font = pygame.font.SysFont("monospace", 20)
        
        self.init_cells()
    
    def init_cells(self):
        x, y, colcount = 0, 0, 0
        for index in range(self.size**2):
            cell = EditModeCell(coords=(x, y))
            self.cells.append( cell )
            if colcount < self.size:
                x += self.tile_size[0]
                colcount += 1
            else:
                x, y, colcount = 0, y+self.tile_size[1], 0
    
    def get_cell(self, mousecoord):
        for cell in self.cells:
            if mousecoord[0] >= cell.coords[0] and mousecoord[0] <= cell.coords[0]+self.tile_size[0]:
                if mousecoord[1] >= cell.coords[1] and mousecoord[1] <= cell.coords[1]+self.tile_size[1]:
                    return (cell)
        return (None)
    
    def remove_uppertile(self, mousecoord):
        cell = self.get_cell(mousecoord)
        if cell and len(cell.tiles) > 0:
            return (cell.tiles.pop())
            
    def add_tile(self, mousecoords, tile_dict):
        '''
            Adiciona um tile em uma celula (celula pode ou nao estar vazia).
            Impede a adicao de tiles repetidos em uma mesma celula
        '''
        for cell in self.cells:
            if mousecoords[0] >= cell.coords[0] and mousecoords[0] <= cell.coords[0]+self.tile_size[0]:
                if mousecoords[1] >= cell.coords[1] and mousecoords[1] <= cell.coords[1]+self.tile_size[1]:
                    if not tile_dict in cell.tiles:
                        cell.tiles.append(tile_dict)
                        return (cell)
        return (None)
    
    def paint(self, screen, grade_color):
        for cell in self.cells:
            if cell.tiles:
                for tile_dict in cell.tiles:
                    screen.blit( tile_dict["tile"], (cell.coords[0], cell.coords[1]) )
            if self.grid_visible:
                pygame.draw.rect(screen, grade_color, (
                                        cell.coords[0], cell.coords[1], self.tile_size[0], self.tile_size[1]), 1)
    
    def fill_row(self, mousecoord, tile_dict):
        for cell in self.cells:
            if mousecoord[1] >= cell.coords[1] and mousecoord[1] <= cell.coords[1]+self.tile_size[1]:
                if tile_dict not in cell.tiles:
                    cell.tiles.append(tile_dict)
    
    def move_cellset_left(self):
        for cell in self.cells:
            x, y = cell.coords[0], cell.coords[1]
            x -= self.tile_size[0]
            cell.coords = (x, y)
    
    def move_cellset_right(self):
        for cell in self.cells:
            x, y = cell.coords[0], cell.coords[1]
            x += self.tile_size[0]
            cell.coords = (x, y)
            
    def move_cellset_up(self):
        for cell in self.cells:
            x, y = cell.coords[0], cell.coords[1]
            y -= self.tile_size[0]
            cell.coords = (x, y)
            
    def move_cellset_down(self):
        for cell in self.cells:
            x, y = cell.coords[0], cell.coords[1]
            y += self.tile_size[0]
            cell.coords = (x, y)
    
    def save_map(self):
        max_x, max_y = 0, 0
        for cell in self.cells:
            if len(cell.tiles):
                if cell.coords[0] > max_x:
                    max_x = cell.coords[0]
                if cell.coords[1] > max_y:
                    max_y = cell.coords[1]
        max_x, max_y = max_x+self.tile_size[0], max_y+self.tile_size[1]
        output = pygame.Surface( (max_x, max_y) )
        output.fill( (255, 0, 255) )
        for cell in self.cells:
            for datatile in cell.tiles:
                output.blit( datatile["tile"], (cell.coords[0], cell.coords[1]) )
        pygame.image.save(output, "teste.bmp")
        return (output)

def edit_mode(engine_properties, tile_management):
    screen = engine_properties.screen
    finished = False
    delay = engine_properties.main_loop_delay
    tile_dict = engine_properties.selected_tile
    multi_tile_dict = engine_properties.multi_selected_tiles
    selected_image = engine_properties.get_selected_image()
    grade_color = engine_properties.grade_editmode_color
    
    if not tile_management:
        tile_management = EditModeTileManagement( engine_properties.editmode_grade_size, selected_image.tile_size )
    
    screen.fill( engine_properties.bg_editmode_color )
    pygame.display.update()
    while not finished:
        
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                keymods = pygame.key.get_mods()
                if keymods == KMOD_LSHIFT or keymods == KMOD_RSHIFT:
                    if event.key == K_LEFT:
                        tile_management.move_cellset_left()
                    
                    elif event.key == K_RIGHT:
                        tile_management.move_cellset_right()
                    
                    elif event.key == K_UP:
                        tile_management.move_cellset_up()
                    
                    elif event.key == K_DOWN:
                        tile_management.move_cellset_down()
                
                elif keymods == KMOD_LCTRL or keymods == KMOD_RCTRL:
                    if event.key == K_s:
                        out=tile_management.save_map()
                        screen.fill( (0,0,0) )
                        screen.blit(out, (0, 40))
                        
                        text = tile_management.font.render("Arquivo salvo com sucesso!", 1, (255, 255, 255))
                        screen.blit(text, (0, 0))
                        
                        pygame.display.update()
                        pygame.time.delay(2000)
                    
                else:
                    if event.key == engine_properties.key_edit_mode or event.key == K_ESCAPE:
                        finished = True
                    
                    elif event.key == K_SPACE:
                        tile_management.grid_visible = not tile_management.grid_visible
                    
                    elif event.key == K_d:
                        tile_management.remove_uppertile(pygame.mouse.get_pos())
                    
            elif event.type == KEYUP:
                keymods = pygame.key.get_mods()
                if keymods == KMOD_LSHIFT or keymods == KMOD_RSHIFT:
                    pass
                else:
                    pass
            
            elif event.type == QUIT:
                finished = True
            
            elif event.type == MOUSEMOTION:
                keymods = pygame.key.get_mods()
                keypressed = pygame.key.get_pressed()
                
                if keymods == KMOD_LCTRL or keymods == KMOD_RCTRL:
                    if len(multi_tile_dict) == 0:
                        tile_management.add_tile(pygame.mouse.get_pos(), tile_dict)
                    else:
                        '''
                            Adicao de multiplos tiles
                        '''
                        tile_size = engine_properties.get_selected_image().tile_size
                        first_tile = get_first_tile(multi_tile_dict)
                        for tile in multi_tile_dict:
                            if tile == first_tile:
                                tilex, tiley = mouse_coord[0], mouse_coord[1]
                            else:
                                # calculo das coordenadas (x,y) do tile em relacao ao primeiro tile
                                if tile["xcoord-orig"] > first_tile["xcoord-orig"]:
                                    tilex = (tile["xcoord-orig"]-first_tile["xcoord-orig"])+mouse_coord[0]
                                elif tile["xcoord-orig"] < first_tile["xcoord-orig"]:
                                    tilex = (first_tile["xcoord-orig"]-tile["xcoord-orig"])-mouse_coord[0]
                                else:
                                    tilex = mouse_coord[0]
                                
                                if tile["ycoord-orig"] > first_tile["ycoord-orig"]:
                                    tiley = (tile["ycoord-orig"]-first_tile["ycoord-orig"])+mouse_coord[1]
                                elif tile["ycoord-orig"] < first_tile["ycoord-orig"]:
                                    tiley = (first_tile["ycoord-orig"]-tile["ycoord-orig"])-mouse_coord[1]
                                else:
                                    tiley = mouse_coord[1]
                            tile_management.add_tile((tilex, tiley), tile)
                            
                
                elif keypressed[K_d]:
                    tile_management.remove_uppertile(pygame.mouse.get_pos())
            
            if pygame.key.get_pressed()[K_l]:
                tile_management.fill_row(pygame.mouse.get_pos(), tile_dict)
            
        if tile_dict and "tile" in tile_dict or len(multi_tile_dict) > 0:
            mouse_coord = pygame.mouse.get_pos()
            screen.fill(engine_properties.bg_editmode_color)
            
            tile_management.paint(screen, grade_color)
            
            if len(multi_tile_dict) == 0:
                screen.blit( tile_dict["tile"], (mouse_coord[0], mouse_coord[1]) )
            else:
                tile_size = engine_properties.get_selected_image().tile_size
                first_tile = get_first_tile(multi_tile_dict)
                for tile in multi_tile_dict:
                    if tile == first_tile:
                        screen.blit( tile["tile"], (mouse_coord[0], mouse_coord[1]) )
                    else:
                        if tile["xcoord-orig"] > first_tile["xcoord-orig"]:
                            tilex = (tile["xcoord-orig"]-first_tile["xcoord-orig"])+mouse_coord[0]
                        elif tile["xcoord-orig"] < first_tile["xcoord-orig"]:
                            tilex = (first_tile["xcoord-orig"]-tile["xcoord-orig"])-mouse_coord[0]
                        else:
                            tilex = mouse_coord[0]
                        
                        if tile["ycoord-orig"] > first_tile["ycoord-orig"]:
                            tiley = (tile["ycoord-orig"]-first_tile["ycoord-orig"])+mouse_coord[1]
                        elif tile["ycoord-orig"] < first_tile["ycoord-orig"]:
                            tiley = (first_tile["ycoord-orig"]-tile["ycoord-orig"])-mouse_coord[1]
                        else:
                            tiley = mouse_coord[1]
                            
                        screen.blit( tile["tile"], (tilex, tiley) )
                        
            
            pygame.display.update()
        pygame.time.delay( delay )
        
    return (tile_management)
