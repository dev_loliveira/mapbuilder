from getopt import getopt

def parse(input_array, search_arguments):
    '''
        input_array ['']
        search_arguments ['']
        Example:
            input_array = ['--argA', '--argB=1', --argc', '2']
            search_array = ['argA', 'argB=', 'argC=']
    '''
    args = getopt(input_array, '', search_arguments)
    opt_dict = {};
    for i in args[0]:
        option = i[0]
        value = i[1]
        option = option.replace('--', '')
        opt_dict[option] = value
    return (opt_dict)

