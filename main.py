import os
import sys

try:
    import resource
except:
    pass

import pygame

from pygame.locals import *
from engine_properties import EngineProperties
from image_tiles import ImageTiles
from mapbuilderutils import *
from mapbuilder.edit_mode import edit_mode

# Import the android module. If we can't import it, set it to None - this
# lets us test it, and check to see if we want android-specific behavior.
try:
    import android
except ImportError:
    android = None

try:
    import pygame.mixer as mixer
except ImportError:
    if android is not None:
        import android.mixer as mixer

# Event constant.
TIMEREVENT = pygame.USEREVENT

# The FPS the game runs at.
FPS = 30



def play_sound(filepath):
    mixer.music.load(filepath)
    mixer.music.play()


verbose_lastmessage = ""
def print_verbose(message="", status="", space_chr="."):
    import console
    global verbose_lastmessage
    if message:
        verbose_lastmessage = message
        for i in range(len(message)):
            char = message[i]
            sys.stdout.write(message[i])
    elif status:
        lastmessage_length = len(verbose_lastmessage)
        max_width = console.getTerminalSize()[0]
        for a in range(max_width-lastmessage_length-len(status)): sys.stdout.write(space_chr)
        sys.stdout.write( ("%s")%(status) )


def main(args_dict={}):
    os.chdir(os.path.dirname(os.path.realpath(__file__))) # altero o diretorio para o diretorio onde o arquivo esta sendo interpretado

    verbose = True if "verbose" in args_dict else False
    debug = True if "debug" in args_dict and args_dict["debug"].lower() == "on" else False
    
    pygame.init()
    
    # Map the back button to the escape key.
    if android:
        android.init()
        android.map_key(android.KEYCODE_BACK, pygame.K_ESCAPE)
        
    if mixer:
        mixer.init()
    
    

    font = pygame.font.Font("media/Font/agentorange.ttf", 20)
    
    if "with-sound" in args_dict:
        if verbose: print_verbose(message="Searching for music file")
        play_sound( ("%s%s%s")%("media", os.sep, "compression_of_time.mp3") )
        if verbose: print_verbose(status="[ OK ]")
    elif verbose:
        print_verbose(message="No sound")

    editmode_tile_management = None
    multi_tile_selection_coords = []
    engine_properties = EngineProperties()
    engine_properties.screen = pygame.display.set_mode( engine_properties.screen_size )
    
    if verbose: print_verbose(message="Loading image files")
    loaded_images = load_images( get_img_files(engine_properties) )
    for datadict in loaded_images:
        data = {}
        if "image" in datadict:
            data["image"] = datadict["image"]
        if "tile_size" in datadict:
            data["tile_size"] = int(datadict["tile_size"])
        if "colorkey" in datadict:
            data["colorkey"] = tuple( [int(value) for value in datadict["colorkey"].split(",")] )
        
        if "image" in data and "tile_size" in data:
            imagetile = ImageTiles( data )
            engine_properties.loaded_images.append( imagetile )
    if verbose: print_verbose(status="[ OK ]")
    
    # Use a timer to control FPS.
    pygame.time.set_timer(TIMEREVENT, 1000 / FPS)
    if verbose: print_verbose(message="Start main loop")
    while not engine_properties.finished:
        selected_image = engine_properties.get_selected_image()
        
        event = pygame.event.wait()
        # print event.type
        
        # Android-specific:
        if android:
            if android.check_pause():
                android.wait_for_resume()
        
        if event.type == QUIT:
            engine_properties.finished = True
        
        elif event.type == pygame.MOUSEBUTTONUP:
            pass
        
        #  ********************************************************************
        #  ********************************************************************
        #  ****************EVENTOS RELACIONADOS COM O TECLADO******************
        #  ********************************************************************
        #  ********************************************************************
        elif event.type == KEYDOWN:
            keymods = pygame.key.get_mods()
            if keymods == KMOD_LSHIFT  or keymods == KMOD_RSHIFT:
                if event.key == K_LEFT:
                    if engine_properties.selected_image_index > 0:
                        selected_image.reset_tileset_coord()
                        engine_properties.selected_image_index -= 1
                elif event.key == K_RIGHT:
                    if engine_properties.selected_image_index < len(engine_properties.loaded_images)-1:
                        selected_image.reset_tileset_coord()
                        engine_properties.selected_image_index += 1
                
                elif event.key == engine_properties.key_edit_mode:
                    if verbose: print_verbose (message="Enterig edit mode")
                    editmode_tile_management = edit_mode(engine_properties, editmode_tile_management)
                    if verbose: print_verbose (status="[ OK ]")
            
            else:
                if event.key == K_LEFT:
                    selected_image.move_tileset_left()
                    
                elif event.key == K_RIGHT:
                    selected_image.move_tileset_rigth()
                    
                elif event.key == K_UP:
                    selected_image.move_tileset_up()
                    
                elif event.key == K_DOWN:
                    selected_image.move_tileset_down()
                
                elif event.key == K_SPACE:
                    engine_properties.grade_screen = not engine_properties.grade_screen
                    
                elif event.key == K_ESCAPE:
                    engine_properties.finished = True
                
                elif event.key == engine_properties.key_edit_mode:
                    if engine_properties.selected_tile:
                        if verbose: print_verbose (message="Entering edit mode.")
                        editmode_tile_management = edit_mode(engine_properties, editmode_tile_management)
                        if verbose: print_verbose (status="[ OK ]")
                
        elif event.type == KEYUP:
            pass
        
        elif event.type == TIMEREVENT:
            screen = engine_properties.screen
            screen_size = engine_properties.screen_size
            screen.fill( engine_properties.bg_color )
            
            selected_image.draw( engine_properties )
            
            rect_focused_tile(engine_properties)
            if multi_tile_selection_coords:
                engine_properties.multi_tile_selection(multi_tile_selection_coords)
                rect_multi_selected_tiles(engine_properties)
            
            engine_properties.selected_tile = selected_image.get_focused_tile(pygame.mouse.get_pos())
            if engine_properties.selected_tile:
                screen.blit( engine_properties.selected_tile["tile"], (screen_size[0]-selected_image.tile_size[0], screen_size[1]-selected_image.tile_size[1]) )
        
            if debug and android is None:
                rusage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1000
                text = font.render( ("Memory usage: %d MB")%(rusage), 1, (255, 255, 255) )
                screen.blit(text, (0, engine_properties.screen_size[1]-20))
            
            pygame.display.update()
            pygame.time.delay( engine_properties.main_loop_delay )
        
        keypressed = pygame.key.get_pressed()
        if keypressed[K_LSHIFT] or keypressed[K_RSHIFT]:
            multi_tile_selection_coords.append(pygame.mouse.get_pos())
        else:
            multi_tile_selection_coords = []
            engine_properties.multi_selected_tiles = []

    sys.stdout.write("\nUninitializing editor")


if __name__ == "__main__": # Executando fora do android
    main({"with-sound": "media/compression_of_time.mp3", "debug": "on"})
