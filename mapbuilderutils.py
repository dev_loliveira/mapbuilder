import os
import pygame

def get_img_files(engine_properties):
    results = []
    accepted_ext      = engine_properties.accepted_image_extensions
    base_dir          = engine_properties.base_dir
    resource_dirs     = engine_properties.resource_dirs
    if len(resource_dirs) > 0:
        for dirname in resource_dirs:
            dirpath = base_dir+os.sep+dirname
            for objname in os.listdir(dirpath):
                filepath = ("%s%s%s")%(dirpath, os.sep, objname)
                if not os.path.isdir(filepath):
                    ext = objname.split(".")[1] if len(objname.split(".")) > 1 else None
                    if ext in accepted_ext or len(accepted_ext) == 0:
                        results.append( filepath )
        return ( results )


def load_images(filepath_list):
    '''
        @return Array
        @description
            Retorna um array de dicionarios contendo
            as configuracoes lidas no arquivo de
            configuracoes da imagem
    '''
    results = []
    for filepath in filepath_list:
        filename = filepath.split(os.sep)[len(filepath.split(os.sep))-1]
        confpath = ("%s%s%s")%(os.path.dirname(filepath), os.sep, filename.split(".")[0])
        if os.path.isfile(confpath):
            conf = read_image_configuration_file(confpath)
            if conf:
                datadict = {}
                datadict["image"] = pygame.image.load(filepath)
                for key in conf:
                    datadict[key] = conf[key]
                results.append( datadict )
    return (results)


def read_image_configuration_file(confpath):
    if not os.path.isfile(confpath): return (None)
    contents = open(confpath, "r").read().split("\n")
    result = {}
    for line in contents:
        if line:
            parameter, value = line.split("=")[0], line.split("=")[1]
            result[parameter] = value
    return (result)


def rect_focused_tile(engine_properties):
    screen = engine_properties.screen
    screen_size = engine_properties.screen_size
    selected_image = engine_properties.get_selected_image()
    
    focused_tile = selected_image.get_focused_tile(pygame.mouse.get_pos())
    if focused_tile:
        pygame.draw.rect(screen, engine_properties.tile_focus_color, (
                                                            focused_tile["xcoord"], focused_tile["ycoord"], 
                                                            selected_image.tile_size[0], selected_image.tile_size[1]), 1)


def rect_multi_selected_tiles(engine_properties):
    if len(engine_properties.multi_selected_tiles) == 0: return (None)
    
    screen = engine_properties.screen
    screen_size = engine_properties.screen_size
    tiles = engine_properties.multi_selected_tiles
    tile_size = engine_properties.get_selected_image().tile_size
    
    for tile in tiles:
        pygame.draw.rect(screen, engine_properties.tile_focus_color, (
                                                            tile["xcoord"], tile["ycoord"], 
                                                            tile_size[0], tile_size[1]), 1)

def get_first_tile(tiles_dict):
    result = None
    for tile in tiles_dict:
        if result == None: result = tile
        if result["xcoord"] > tile["xcoord"] or result["ycoord"] > tile["ycoord"]:
            result = tile
    return (result)